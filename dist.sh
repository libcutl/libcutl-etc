#! /usr/bin/env bash

# Create libcutl source distributions with the autotools build system.
#
# -cc <c-compiler>
# -cxx <c++-compiler>
# -cxxflags <c++-compiler-flags>
# -out <built-output-directory>, default is libcutl-X.Y.Z in current dir.
# -no-check - do not check the distribution
# -j <jobs>
#

trap 'exit 1' ERR

function error ()
{
  echo "$*" 1>&2
}

wd=`pwd`
v=`cat libcutl/version`
cc=gcc
cxx=g++
cxxflags="-W -Wall -O3"
jobs=22
out=
dist_target=distcheck

while [ $# -gt 0 ]; do
  case $1 in
    -cc)
      shift
      cc=$1
      shift
      ;;
    -cxx)
      shift
      cxx=$1
      shift
      ;;
    -cxxflags)
      shift
      cxxflags=$1
      shift
      ;;
    -out)
      shift
      out=$1
      shift
      ;;
    -no-check)
      dist_target=dist
      shift
      ;;
    -j)
      shift
      jobs=$1
      shift
      ;;
    *)
      error "unknown option: $1"
      exit 1
      ;;
  esac
done

if [ "$out" = "" ]; then
  out=$wd/libcutl-$v
fi

echo "packaging libcutl-$v"

rm -rf $out

make -C libcutl dist dist_prefix=$out
cd $out
./bootstrap
./configure CC="$cc" CXX="$cxx" CXXFLAGS="$cxxflags"
make -j $jobs
make $dist_target
cp libcutl-$v.zip ../
cp libcutl-$v.tar.gz ../
cp libcutl-$v.tar.bz2 ../
cd ..

sha1sum libcutl-$v.zip >libcutl-$v.zip.sha1
sha1sum libcutl-$v.tar.gz >libcutl-$v.tar.gz.sha1
sha1sum libcutl-$v.tar.bz2 >libcutl-$v.tar.bz2.sha1

cd $wd
