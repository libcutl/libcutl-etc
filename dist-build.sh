#! /usr/bin/env bash

# Create libcutl-x.y.z directory with the 'build' build system.
#

trap 'exit 1' ERR

v=`cat libcutl/version.txt`

echo "packaging libcutl-$v"
echo "EVERYTHING MUST BE COMMITTED!"

# prepare libcutl-x.y.z
#
rm -rf libcutl-$v
mkdir libcutl-$v
cd libcutl
git archive legacy | tar -x -C ../libcutl-$v
cd ..
rm -f libcutl-$v/.gitignore libcutl-$v/.gitattributes
